let dbPool = require('../core/DbConnection')
let { onlyUnique } = require('../core/util')

async function search(what, where) {
    let result = []

    if (what !== undefined) {
        console.log("Found what value")

        let anagrafiche = await dbPool.query(
            `SELECT id, ragione_sociale from anagrafiche where ragione_sociale 
            like "%${what}%" limit 0,10`
        )
        anagrafiche && result.push({ anagrafiche })

        let macro = await dbPool.query(
            `SELECT id, macro_name_EN from macro where macro_name_EN like "%${what}%" 
            or macro_name_IT like "%${what}%" limit 0,10`
        )
        macro && result.push({ macro })

        let micro = await dbPool.query(
            `SELECT id, name_EN from micro 
            where name_IT like "%${what}%"
            or name_EN like "%${what}%"
            or name_ES like "%${what}%"
            or name_FR like "%${what}%" limit 0,10
            `
        )
        micro && result.push({ micro })


    }
    if (where !== undefined) {

        let nazioni = await dbPool.query(
            `SELECT name_EN from nazioni 
            where name_IT like "%${where}%"
            or name_EN like "%${where}%"
            or name_ES like "%${where}%"
            or name_FR like "%${where}%" limit 0,10
            `
        )
        nazioni && result.push({ nazioni })

        let admin1_regioni = await dbPool.query(
            `SELECT admin1_name from admin1_regioni 
            where admin1_name like "%${where}%" limit 0,10`
        )

        admin1_regioni && result.push({ admin1_regioni })

        let admin2_province = await dbPool.query(
            `SELECT admin2_name from admin2_province 
            where admin2_name like "%${where}%" limit 0,10`
        )

        admin2_province && result.push({ admin2_province })
    }

    let localita = await dbPool.query(
        `SELECT id, name from localita limit 0,10`
    )
    localita && result.push({ localita })

    if (!result) {
        throw new Error("404")
    }
    return result
}



async function fullTextSearch(what, where) {

    let result = {}

    if (what !== undefined) {
        console.log("Found what value")

        let anagrafiche = await dbPool.query(
            `SELECT 
            id,
            MATCH (ragione_sociale) AGAINST ('${what.split(" ").join(",")}' IN NATURAL LANGUAGE MODE) AS Point,
            ragione_sociale,
            indirizzo,
            cap,
            id_localita,
            id_prov,
            id_regio,
            id_macro,
            id_micro,
            country_code
        FROM
            anagrafiche
        WHERE
            MATCH (ragione_sociale) AGAINST ('${what.split(" ").join(",")}' IN NATURAL LANGUAGE MODE)
        ORDER BY Point DESC limit 0, 50`
        )

        result.anagrafiche = anagrafiche

        let macro = await dbPool.query(
            `SELECT 
            id,
            macro_name_EN
            FROM
                macro
            where id IN(${anagrafiche.map(x => x.id_macro).filter(onlyUnique)})
            limit 0, 50`
        )

        result.macro = macro

        let micro = await dbPool.query(
            `SELECT 
            id,
            name_EN
            FROM
                micro
            where id IN(${anagrafiche.map(x => x.id_micro).filter(onlyUnique)})
            limit 0, 50`
        )

        result.micro = micro

        let localita = await dbPool.query(
            `SELECT 
            id,
            name
            FROM
            localita
            where id IN(${anagrafiche.map(x => "'" + x.id_localita + "'").filter(onlyUnique)})
            limit 0, 50`
        )

        result.localita = localita

        let nazioni = await dbPool.query(
            `SELECT 
            id,
            name_EN
            FROM
            nazioni
            where country_code IN(${anagrafiche.map(x => "'" + x.country_code + "'").filter(onlyUnique)})
            limit 0, 50`
        )

        result.nazioni = nazioni

        let admin1_regioni = await dbPool.query(
            `SELECT 
            id,
            admin1_name
            FROM
            admin1_regioni
            where id IN(${anagrafiche.map(x => "'" + x.id_regio + "'").filter(onlyUnique)})
            limit 0, 50`
        )

        result.admin1_regioni = admin1_regioni

        let admin2_province = await dbPool.query(
            `SELECT 
            id,
            admin2_name
            FROM
            admin2_province
            where id IN(${anagrafiche.map(x => "'" + x.id_prov + "'").filter(onlyUnique)})
            limit 0, 50`
        )

        result.admin2_province = admin2_province
    }

    if (!result) {
        throw new Error("404")
    }

    anagrafiche = result.anagrafiche.map(an => {
        return {
            id: an.id,
            ragione_sociale: an.ragione_sociale,
            indirizzo: an.indirizzo,
            cap: an.cap,
            id_localita: an.id_localita,
            id_prov: an.id_prov,
            id_regio: an.id_regio
        }
    })
    result.anagrafiche = anagrafiche;
    console.log(result);
    return result
}

module.exports = { search, fullTextSearch }