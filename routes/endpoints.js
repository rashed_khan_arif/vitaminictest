var express = require('express');
var router = express.Router();
const { search, fullTextSearch } = require('../services/endpointService')

router.get("/endpoint1", async (req, res, next) => {
    const what = req.query.what;
    const where = req.query.where;
    console.log('What: ' + what)
    console.log('Where: ' + where)
    try {
        const result = await search(what, where);
        console.log(result);
        res.json(result);
    } catch (err) {
        res.json(err);
    }
});

router.get("/endpoint2", async (req, res, next) => {
    const what = req.query.what;
    const where = req.query.where;
    console.log('What: ' + what)
    console.log('Where: ' + where)
    try {
        const result = await fullTextSearch(what, where);
        console.log(result);
        res.json(result);
    } catch (err) {
        res.json(err);
    }
});

module.exports = router;